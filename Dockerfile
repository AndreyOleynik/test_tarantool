FROM tarantool/tarantool:1.7.4
COPY init.lua /opt/tarantool/init.lua
CMD ["tarantool", "/opt/tarantool/init.lua"]