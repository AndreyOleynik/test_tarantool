import pytest
import tarantool
import time


@pytest.fixture
def tarantool_client():
    client = tarantool.connect(port=3301)
    return client


@pytest.mark.xfail
def test_without_sleep(tarantool_client):
    # insert record
    insert_timestamp = time.time()
    tarantool_client.space('history').insert((1, insert_timestamp))

    # get record back
    response = tarantool_client.call('get_messages', (1, 10))
    message_timestamps = [msg[1] for msg in response.data]

    assert insert_timestamp in message_timestamps


def test_with_sleep(tarantool_client):
    # insert record
    insert_timestamp = time.time()
    tarantool_client.space('history').insert((1, insert_timestamp))

    # waiting
    time.sleep(2)

    # get record back
    response = tarantool_client.call('get_messages', (1, 10))
    message_timestamps = [msg[1] for msg in response.data]

    assert insert_timestamp in message_timestamps
