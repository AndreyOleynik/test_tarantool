box.cfg {
    listen = '*:3301';
    slab_alloc_arena = 0.5;
    slab_alloc_minimal = 16;
    slab_alloc_maximal = 1048576;
    slab_alloc_factor = 1.06;
    snapshot_period = 10;
    snapshot_count = 8;
    panic_on_snap_error = true;
    panic_on_wal_error = true;
    rows_per_wal = 5000000;
    snap_io_rate_limit = nil;
    wal_mode = "fsync";
    wal_dir_rescan_delay = 2.0;
    io_collect_interval = nil;
    readahead = 16320;
    log_level = 5;
    logger_nonblock = true;
    too_long_threshold = 0.5;
}

local function bootstrap()
    local space = box.schema.create_space('history', {engine='vinyl'})
    space:create_index('primary', {type='tree', parts={1, 'unsigned', 2, 'number'}})
end

box.once('init', bootstrap)

function get_messages(id, message_count)
   local msg = {}
   local query = box.space.history:pairs({id, os.time()}, {iterator='LE'})

   for k, v in query:take(message_count) do
      msg[#msg+1] = v
   end

   return msg
end
