### Сборка и запуск контейнера
```
docker build -t tarantool .
docker run --rm -p 3301:3301 tarantool
```
### Запуск тестов
```
pip install -r requirements.txt
pytest
```